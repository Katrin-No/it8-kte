import os
from flask import Flask, render_template
from data import Data

app = Flask(__name__)


@app.route("/")
def main():
    """ renders index template """
    data = Data()
    array = ['CPU frequency is {} MHz'.format(data.cpu_frequency), 'RAM usage is {} MB'.format(int(
        data.ram_usage / 1024 / 1024)), 'RAM total is {} MB'.format(int(data.ram_total / 1024 / 1024)), 'RAM usage is {} %'.format(data.ram_usage_pct)]
    return render_template("index.html", array=array)


port = int(os.getenv('PORT', 5000))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=port)
