""" object with current cpu and ram data"""
import sys
import psutil


class Data():
    """ object with cpu and ram data """

    def __init__(self):
        """ it creates an object """
        self.cpu_frequency = int(psutil.cpu_freq().current)
        self.ram_usage = int(psutil.virtual_memory().total -
                             psutil.virtual_memory().available)
        self.ram_total = int(psutil.virtual_memory().total)
        self.ram_usage_pct = psutil.virtual_memory().percent

    def results(self):
        """ outputing data """
        print('CPU frequency is {} MHz'.format(self.cpu_frequency))
        print('RAM usage is {} MB'.format(int(self.ram_usage / 1024 / 1024)))
        print('RAM total is {} MB'.format(int(self.ram_total / 1024 / 1024)))
        print('RAM usage is {} %'.format(self.ram_usage_pct))

    def save_results(self):
        """ saving data in csv """
        with open("results.txt", "w") as sys.stdout:
            self.results()
        sys.stdout.close()
