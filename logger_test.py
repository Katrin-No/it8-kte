import unittest
import pdb
from logger import Logger


class TestLogger(unittest.TestCase):

    def test_warning_cpu(self):
        log = Logger()
        # pdb.set_trace()
        self.assertEqual(log.warning_cpu(2500),
                         "⚠️ Alarm: frequency usage is 2500 MHz")
        self.assertEqual(log.warning_cpu(1004),
                         "Warning: frequency usage is 1004 MHz")

    def test_warning_ram(self):
        log = Logger()

        self.assertEqual(log.warning_ram(80),
                         "Warning: ram usage is 80 pct")
