""" main module """

import argparse
from logger import Logger
from data import Data


def info():
    """ Define the program description and initiate the parser with it """
    text = 'This programm will show you current data from your PC \
        like cpu frequency and ram usage'
    parser = argparse.ArgumentParser(description=text)
    parser.parse_args()

    # parser = argparse.ArgumentParser()
    # # Add the parameters
    # parser.add_argument(
    #     "parameter", help="c for cpu_frequency, r for ram usage, a for all, ", type=str)
    # # Parse the arguments
    # args = parser.parse_args()
    # data = Data()
    # if args.parameter == "c":
    #     print('CPU frequency is {} MHz'.format(data.cpu_frequency))
    # elif args.parameter == "r":
    #     print('RAM usage is {} %'.format(data.ram_usage_pct))
    # elif args.parameter == "a":
    #     data.results()
    #     data.save_results()
    # else:
    #     print("Wrong parameter")


def interactive():
    """ ask user to choose data to output and shows it """
    number = int(input(
        "Type: \n 1 to know your current CPU frequency \n 2 for RAM usage in percent \n 3 for all \
            \n: "))

    data = Data()

    if number == 1:
        print('CPU frequency is {} MHz'.format(data.cpu_frequency))
    elif number == 2:
        print('RAM usage is {} %'.format(data.ram_usage_pct))
    elif number == 3:
        data.results()
        data.save_results()
    else:
        print("Wrong number")


def main():
    """ create logger, print and save monitoring data """
    info()

    logger = Logger()
    logger.handler()

    interactive()

    data = Data()
    logger.warning_cpu(data.cpu_frequency)
    logger.warning_ram(data.ram_usage_pct)


if __name__ == "__main__":
    main()
