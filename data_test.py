import unittest
from unittest.mock import patch
import data


class TestData(unittest.TestCase):
    """using the mock.patch function"""

    @patch.object(data.Data, 'results')
    def test_results(self, mock_results):
        mock_results.return_value = "CPU frequency is 1921 MHz"
        data_class = data.Data()

        output = data_class.results()
        expected = "CPU frequency is 1921 MHz"

        self.assertEqual(output, expected)
