""" logger with messages """
import logging


class Logger():
    """ logger with messages """

    def __init__(self):
        """ create logger """
        self.logger = logging.getLogger('mylogger')

    def handler(self):
        """ create logging file """
        handler = logging.FileHandler('errorhandler.log')
        self.logger.addHandler(handler)

    def warning_cpu(self, cpu_frequency):
        """ limit for monitoring values: cpu_frequency """
        if cpu_frequency > 2000:
            self.logger.warning("⚠️ Alarm: frequency usage is %s MHz",
                                cpu_frequency)
            return "⚠️ Alarm: frequency usage is "+str(cpu_frequency)+" MHz"
        if cpu_frequency > 1000:
            self.logger.warning("Warning: frequency usage is %s MHz",
                                cpu_frequency)
            return "Warning: frequency usage is "+str(cpu_frequency)+" MHz"
        return None

    def warning_ram(self, ram_usage_pct):
        """ limit for monitoring values: ram_usage """
        if ram_usage_pct > 10:
            self.logger.warning(
                "Warning: ram usage is %s pct", ram_usage_pct)
            return "Warning: ram usage is "+str(ram_usage_pct)+" pct"
        return None


# logger.warning('This is a WARNING message')
# logger.error('This is an ERROR message')
# logger.critical('This is a CRITICAL message')
